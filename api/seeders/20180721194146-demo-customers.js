'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.bulkInsert('customers', [{
      default_address_id: 1,
      gender: 'M',
      first_name: 'Hardev',
      last_name: 'Sharma',
      dob: '1986-09-27',
      email_address: 'hardev@gmail.com',
      telephone: '9876528433',
      fax: '1234567890',
      password: '123456',
      date_of_last_logon: '2018-07-22',
      number_of_logons: 10,
      password_reset_key: 'fsd1s2fs1f2sdf45',
      password_reset_date: '2018-07-22',
      createdAt: '2018-07-22',
      updatedAt: '2018-07-22'
    },
    {
      default_address_id: 1,
      gender: 'M',
      first_name: 'Ravinder',
      last_name: 'Kumar',
      dob: '1996-09-27',
      email_address: 'ravi@gmail.com',
      telephone: '9876528433',
      fax: '1234567890',
      password: '123456',
      date_of_last_logon: '2018-07-22',
      number_of_logons: 10,
      password_reset_key: 'fgh45fg54hfg45hfghf45',
      password_reset_date: '2018-07-22',
      createdAt: '2018-07-22',
      updatedAt: '2018-07-22'
    }], {});
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.bulkDelete('customers', null, {});
  }
};
