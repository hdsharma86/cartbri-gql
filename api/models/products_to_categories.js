'use strict';
module.exports = (sequelize, DataTypes) => {
  var products_to_categories = sequelize.define('products_to_categories', {
    products_id: DataTypes.INTEGER,
    categories_id: DataTypes.INTEGER
  }, {});
  products_to_categories.associate = function(models) {
    // associations can be defined here
  };
  return products_to_categories;
};