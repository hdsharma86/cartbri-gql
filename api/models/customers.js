'use strict';
module.exports = (sequelize, DataTypes) => {
  var customers = sequelize.define('customers', {
    default_address_id: DataTypes.INTEGER,
    gender: DataTypes.STRING,
    first_name: DataTypes.STRING,
    last_name: DataTypes.STRING,
    dob: DataTypes.DATE,
    email_address: DataTypes.STRING,
    telephone: DataTypes.STRING,
    fax: DataTypes.STRING,
    password: DataTypes.STRING,
    date_of_last_logon: DataTypes.DATE,
    number_of_logons: DataTypes.INTEGER,
    password_reset_key: DataTypes.STRING,
    password_reset_date: DataTypes.DATE
  }, {});
  customers.associate = function(models) {
    // associations can be defined here
  };
  return customers;
};