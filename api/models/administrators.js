'use strict';
module.exports = (sequelize, DataTypes) => {
  var administrators = sequelize.define('administrators', {
    username: DataTypes.STRING,
    password: DataTypes.STRING
  }, {});
  administrators.associate = function(models) {
    // associations can be defined here
  };
  return administrators;
};