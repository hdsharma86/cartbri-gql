'use strict';
module.exports = (sequelize, DataTypes) => {
  var customers_basket = sequelize.define('customers_basket', {
    customers_id: DataTypes.INTEGER,
    products_id: DataTypes.INTEGER,
    basket_quantity: DataTypes.INTEGER,
    final_price: DataTypes.DOUBLE
  }, {});
  customers_basket.associate = function(models) {
    // associations can be defined here
  };
  return customers_basket;
};