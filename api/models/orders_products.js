'use strict';
module.exports = (sequelize, DataTypes) => {
  var orders_products = sequelize.define('orders_products', {
    orders_id: DataTypes.INTEGER,
    products_id: DataTypes.INTEGER,
    products_model: DataTypes.STRING,
    products_name: DataTypes.STRING,
    products_price: DataTypes.DOUBLE,
    final_price: DataTypes.DOUBLE,
    products_tax: DataTypes.DOUBLE,
    products_quantity: DataTypes.INTEGER
  }, {});
  orders_products.associate = function(models) {
    // associations can be defined here
  };
  return orders_products;
};