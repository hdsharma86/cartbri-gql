'use strict';
module.exports = (sequelize, DataTypes) => {
  var orders = sequelize.define('orders', {
    customers_id: DataTypes.INTEGER,
    customers_name: DataTypes.STRING,
    customers_company: DataTypes.STRING,
    customers_street_address: DataTypes.STRING,
    customers_suburb: DataTypes.STRING,
    customers_city: DataTypes.STRING,
    customers_postcode: DataTypes.STRING,
    customers_state: DataTypes.STRING,
    customers_country: DataTypes.STRING,
    customers_telephone: DataTypes.STRING,
    customers_email_address: DataTypes.STRING,
    customers_address_format_id: DataTypes.INTEGER,
    delivery_name: DataTypes.STRING,
    delivery_company: DataTypes.STRING,
    delivery_street_address: DataTypes.STRING,
    delivery_suburb: DataTypes.STRING,
    delivery_city: DataTypes.STRING,
    delivery_postcode: DataTypes.STRING,
    delivery_state: DataTypes.STRING,
    delivery_country: DataTypes.STRING,
    delivery_address_format_id: DataTypes.INTEGER,
    billing_name: DataTypes.STRING,
    billing_company: DataTypes.STRING,
    billing_street_address: DataTypes.STRING,
    billing_suburb: DataTypes.STRING,
    billing_city: DataTypes.STRING,
    billing_postcode: DataTypes.STRING,
    billing_state: DataTypes.STRING,
    billing_country: DataTypes.STRING,
    billing_address_format_id: DataTypes.INTEGER,
    payment_method: DataTypes.STRING,
    cc_type: DataTypes.STRING,
    cc_owner: DataTypes.STRING,
    cc_number: DataTypes.STRING,
    cc_expires: DataTypes.DATE,
    orders_status: DataTypes.INTEGER,
    currency: DataTypes.STRING,
    currency_value: DataTypes.DOUBLE
  }, {});
  orders.associate = function(models) {
    // associations can be defined here
  };
  return orders;
};