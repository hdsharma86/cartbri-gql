const { resolver } = require('graphql-sequelize');
const { GraphQLSchema, GraphQLObjectType, GraphQLString, GraphQLInt, GraphQLNonNull, GraphQLList } = require('graphql');
const { customers } = require('../models');

//Define User type
const customersType = new GraphQLObjectType({
  name: 'customers',
  description: 'A user',
  fields: {
    id: {
      type: new GraphQLNonNull(GraphQLInt),
      description: 'The id of the user.',
    },
    email_address: {
      type: GraphQLString,
      description: 'The name of the user.',
    }
  }
})

//schema
const schema = new GraphQLSchema({
  query: new GraphQLObjectType({
    name: 'root',
    fields: {
      customers: {
        type: new GraphQLList(customersType),
        resolve: resolver(customers)
      }
    }
  })
})

module.exports = schema