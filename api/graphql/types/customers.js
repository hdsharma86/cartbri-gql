'use strict';

const {
  GraphQLObjectType,
  GraphQLString,
  GraphQLID,
  GraphQLList
}  = require('graphql');

module.exports = new GraphQLObjectType({
  name: 'Customers',
  description: 'customers of some quote',
  fields () {
      return {
          id: {
              type: GraphQLID,
              description: "customer's ID",
              resolve (customers) {
                return customers.id;
              }
          },
          first_name: {
              type: GraphQLString,
              description: "customer's first name",
              resolve (customers) {
                return customers.first_name;
              }
          },
          last_name: {
              type: GraphQLString,
              description: "customer's last name",
              resolve (customers) {
                return customers.first_name;
              }
          },
      }
  }
});