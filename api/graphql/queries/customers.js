'use strict';

const { resolver } = require('graphql-sequelize');
const { GraphQLSchema, GraphQLObjectType, GraphQLString, GraphQLInt, GraphQLNonNull, GraphQLList } = require('graphql');
const { customers } = require('../../models');
const customersType = require('../types/customers');

const schema = new GraphQLSchema({
  query: new GraphQLObjectType({
    name: 'root',
    fields: {
      customers: {
        type: new GraphQLList(customersType),
        resolve: resolver(customers)
      },
      customer: {
        type: customersType,
        args: {
          id: {
            description: 'id of the user',
            type: new GraphQLNonNull(GraphQLString)
          }
        },
        resolve: (root, args) => {
          return customers.findAll({where: args});
        }
      }
    }
  })
})

module.exports = schema;