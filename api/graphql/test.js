const { resolver } = require('graphql-sequelize');
const { GraphQLSchema, GraphQLObjectType, GraphQLString, GraphQLInt, GraphQLNonNull, GraphQLList } = require('graphql');
const { customers } = require('../models');
const customersType = require('./types/customers');

//schema
const schema = new GraphQLSchema({
  query: new GraphQLObjectType({
    name: 'root',
    fields: {
      customers: {
        type: new GraphQLList(customersType),
        resolve: resolver(customers)
      }
    }
  })
})

module.exports = schema