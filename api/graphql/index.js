const graphql = require('graphql');
//const connectionString = 'myURI';
//const pgp = require('pg-promise')();
const db = {}
//db.conn = pgp(connectionString);
const queries  = require('./queries');

const {
   GraphQLObjectType,
   GraphQLID,
   GraphQLString,
   GraphQLBoolean,
   GraphQLList,
   GraphQLSchema,
   buildSchema
} = graphql;

const RootQuery = new GraphQLSchema({
    query: new GraphQLObjectType({
        name: 'RootQueryType',
        fields: queries
    }),
    /*mutation: new GraphQLObjectType({
        name: 'Mutation',
        fields: mutations
    })*/
});

module.exports = new GraphQLSchema({
    query: RootQuery
});

/*const CustomerType = new GraphQLObjectType({
    name: 'customers',
    fields: () => ({
       name: { type: GraphQLString }
    })
 })
 
 const RootQuery = new GraphQLObjectType({
    name: 'RootQueryType',
    fields: {
       customers: {
            type: CustomerType,
            resolve(obj, args) {
                return {
                    name: "Hardev Sharma"
                }
            }
        }
    }
 })

module.exports = new GraphQLSchema({
   query: RootQuery
})*/