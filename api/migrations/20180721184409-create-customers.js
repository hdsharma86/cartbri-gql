'use strict';
module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.createTable('customers', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      default_address_id: {
        type: Sequelize.INTEGER
      },
      gender: {
        type: Sequelize.STRING
      },
      first_name: {
        type: Sequelize.STRING
      },
      last_name: {
        type: Sequelize.STRING
      },
      dob: {
        type: Sequelize.DATE
      },
      email_address: {
        type: Sequelize.STRING
      },
      telephone: {
        type: Sequelize.STRING
      },
      fax: {
        type: Sequelize.STRING
      },
      password: {
        type: Sequelize.STRING
      },
      date_of_last_logon: {
        type: Sequelize.DATE
      },
      number_of_logons: {
        type: Sequelize.INTEGER
      },
      password_reset_key: {
        type: Sequelize.STRING
      },
      password_reset_date: {
        type: Sequelize.DATE
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE
      }
    });
  },
  down: (queryInterface, Sequelize) => {
    return queryInterface.dropTable('customers');
  }
};