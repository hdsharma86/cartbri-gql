'use strict';
module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.createTable('orders_products', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      orders_id: {
        type: Sequelize.INTEGER
      },
      products_id: {
        type: Sequelize.INTEGER
      },
      products_model: {
        type: Sequelize.STRING
      },
      products_name: {
        type: Sequelize.STRING
      },
      products_price: {
        type: Sequelize.DOUBLE
      },
      final_price: {
        type: Sequelize.DOUBLE
      },
      products_tax: {
        type: Sequelize.DOUBLE
      },
      products_quantity: {
        type: Sequelize.INTEGER
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE
      }
    });
  },
  down: (queryInterface, Sequelize) => {
    return queryInterface.dropTable('orders_products');
  }
};