const express = require('express');
const logger = require('morgan');
const bodyParser = require('body-parser');
var { buildSchema } = require('graphql');
const GraphHTTP = require('express-graphql');
//const schema = require('./schema');
const schema = require('./graphql/queries/customers');

// Set up the express app
const app = express();

// Log requests to the console.
app.use(logger('dev'));

// Parse incoming requests data (https://github.com/expressjs/body-parser)
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));


/* Here a simple schema is constructed using the GraphQL schema language (buildSchema). 
   More information can be found in the GraphQL spec release */

app.use('/graphql', GraphHTTP({
  schema,
  pretty: true,
  graphiql:true
}));

module.exports = app;
